package com.database.migration.example.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EXAMPLE_2", schema = "EXAMPLE_SCHEMA")
public class Example2 implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "ID_EXAMPLE", nullable = false)
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "EXAMPLE_FIELD_1")
  private String exampleField1;

  @Column(name = "EXAMPLE_FIELD_2")
  private String exampleField2;

  @Column(name = "EXAMPLE_FIELD_3")
  private String exampleField3;

  @Column(name = "EXAMPLE_FIELD_4")
  private String exampleField4;

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getExampleField1()
  {
    return exampleField1;
  }

  public void setExampleField1(String exampleField1)
  {
    this.exampleField1 = exampleField1;
  }

  public String getExampleField2()
  {
    return exampleField2;
  }

  public void setExampleField2(String exampleField2)
  {
    this.exampleField2 = exampleField2;
  }

  public String getExampleField3()
  {
    return exampleField3;
  }

  public void setExampleField3(String exampleField3)
  {
    this.exampleField3 = exampleField3;
  }

  public String getExampleField4()
  {
    return exampleField4;
  }

  public void setExampleField4(String exampleField4)
  {
    this.exampleField4 = exampleField4;
  }

}
